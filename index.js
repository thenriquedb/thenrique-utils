function welcome(name) {
  return `Welcome ${name}!`;
}

function getDate() {
  return new Date().getDate().toString();
}

module.exports = { welcome, getDate };
